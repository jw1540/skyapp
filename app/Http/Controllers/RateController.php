<?php

namespace App\Http\Controllers;

use App\Reviews;
use App\Shows;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $shows = Shows::all();
        return view('rate.rate')->withShows($shows);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('upload.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $show = new Shows(array(
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'image' => $request->get('image'),
        ));

        $show->save();

        if(Input::hasFile('image')){
        $imageName = $show->id . '.' .
            $request->file('image')->getClientOriginalExtension();


        $request->file('image')->move(
            base_path() . '/public/images/shows', $imageName

        );

        $show->image = $imageName;
        }

        $show->save();


        Session::flash('flash_message', 'Show successfully added!');

        return redirect(route('rate.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function show(Request $request, $id)
    {

        $show = Shows::findOrFail($id);
        $reviews = Reviews::all();
        $score = 0;
        return view('rate.show')->withShow($show)->withReviews($reviews)->withScore($score);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
