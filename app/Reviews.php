<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Shows;

class Reviews extends Model
{
    protected $table = 'reviews';

    protected $fillable = array(
        'vote',
        'shows_id'
    );

    public function shows(){
        return $this->belongsTo('App\Shows');
    }


}
