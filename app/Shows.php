<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Reviews;

class Shows extends Model
{
    protected $table = 'shows';

    protected $fillable = array(
        'title',
        'description'
    );

    protected $appends = [
        'downvotes',
        'upvotes'
    ];

    public function reviews(){
        return $this->hasMany('App\Reviews');
    }

    public function getUpvotesAttribute()
    {
        return $this->reviews->where('vote', 1)->count();
    }

    public function getDownvotesAttribute()
    {
        return $this->reviews->where('vote', -1)->count();
    }

}
