@extends('layouts.master')
@section('content')
<section class="rated" id="show">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center wow fadeInDown animated">
                <h2 class="section-heading">{{$show->title}}</h2>
                <hr class="colored">
            </div>
            <div class="col-lg-4 text-center col-md-4 col-sm-4 col-xs-12 pull-right media wow zoomIn animated">

                <img style="width: 300px;;" class="img-circle img-me" src="{{ asset('images/shows/' . $show->image) }}">
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left media summernote">
                <h3 class="text-justify">{{$show->description}}</h3>
                <hr class="gradient">
                {!! Form::open(['route' => 'votes.store']) !!}
                {!! Form::button('<i class="fa fa-2x fa-thumbs-o-up"></i>', array('type' => 'submit', 'value' => 'vote', 'name' => 'upvotes')) !!}
                {!! Form::hidden('shows_id', $show->id) !!}
                {{$show->upvotes}}
                {!! Form::button('<i class="fa fa-2x fa-thumbs-o-down"></i>', array('type' => 'submit', 'value' => 'vote', 'name' => 'downvotes')) !!}
                {{$show->downvotes}}
                {!! Form::hidden('show_id', $show->id) !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('#show').offset().top
        }, 'slow');
    });
</script>
@endsection