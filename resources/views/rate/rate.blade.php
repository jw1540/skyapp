@extends('layouts.master')
@section('content')
    <section class="rating">
        <div class="container" id="rate">
            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <h1 class="section-heading text-center" style="font-family: 'Muli', sans-serif">Watch. Rate. Watch again.</h1>
            <div class="row">
                @foreach($shows as $show)
                <div class="col-lg-3" style="margin-top: 10px">
                    <div class="overlay-container">
                        <img class="img-responsive" src="{{ asset('images/shows/' . $show->image) }}"/>
                        <a class="overlay" href="{{route('rate.show', $show->id)}}">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            // Handler for .ready() called.
            $('html, body').animate({
                scrollTop: $('#rate').offset().top
            }, 'slow');
        });
    </script>
@endsection