<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Sky | Rate</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!--- Stylesheets and Fonts -->
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/animate.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{asset('/js/wow.min.js')}}"></script>
    <script>
        new WOW().init();
    </script>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top before-color">
<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand candiman scrollup" href="#"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{route('home')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{route('rate.index')}}"><i class="fa fa-list-ul"></i> Rate</a> </li>
            <li><a href="{{route('shows.index')}}"><i class="fa fa-trophy"></i> Leaderboard</a></li>
            <li><a href="{{route('rate.create')}}"><i class="fa fa-upload"></i> Upload</a></li>
        </ul>
    </div>
</div>
</nav>

<!-- Carousel -->
<header id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="item active autoslide">
            <div class="fill"></div>
        </div>
        <div class="item">
            <div class="fill1"></div>
        </div>
        <div class="item">
            <div class="fill2"></div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>
<!-- Main Content -->
<main>
    @yield('content')
</main>
</body>
<!-- Scroll to Top Java (put in separate file and extend)-->
<script>
    $(document).ready(function () {
        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
    })
</script>

