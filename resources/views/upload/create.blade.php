@extends('layouts.master')
@section('content')
<div class="container">
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
  <h1 class="section-heading text-center" style="font-family: muli, sans-serif" id="create">Add to the collection</h1>
    <div class="row" style="margin-top: 15px">
        <div class="col-lg-6">
            {!! Form::open(['route' => 'rate.store', 'files' => 'true', 'class' => 'cd-form floating-labels']) !!}
            <div class="icon" style="margin-bottom: 10px">
                {!! Form::label('title', 'Title:', ['class' => 'cd-label']) !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
            <div class="summernote" style="margin-top: 10px">
                {!! Form::label('description', 'Description:', ['class' => 'cd-label']) !!}
                {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'summernote']) !!}
            </div>
            <div class="icon">
                {!! Form::label('Image', null, ['class' => 'cd-label']) !!}
                {!! Form::file('image', null) !!}
            </div>
        {!! Form::submit('Add', ['class' => 'btn btn-primary pull-right']) !!}

        {!! Form::close() !!}
        </div>
        <div class="col-lg-6">
            <img src="http://www.sky.com/assets/89faa88c8656513827305bbc68b42ef1.png" style="margin-top: -120px; margin-left: 30px">
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('#create').offset().top
        }, 'slow');
    });
</script>
@endsection

