@extends('layouts.master')
@section('content')
<script src="{{asset('/js/jquery.tablesorter.js')}}"></script>
    <script>
        $(document).ready(function()
                {
                    $(".table").tablesorter();
                }
        );
    </script>
<section class="leader" id="leader">
        <div class="container">
            <h1 class="section-heading text-center" style="font-family: muli, sans-serif">Leaderboard</h1>
            <h2 class="text-center" style="font-family: muli, sans-serif">Click to reorder</h2>
            <hr class="gradient">
            <table class="table table-hover" id="table">
                <thead style="background-color: rgba(4, 137, 177, 0.5)">
                <tr>
                    <th style="cursor: pointer">Film <i class="fa fa-arrows-v"></i></th>
                    <th style="cursor: pointer">Title <i class="fa fa-arrows-v"></i></th>
                    <th style="cursor: pointer">Upvotes <i class="fa fa-arrows-v"></i></th>
                    <th style="cursor: pointer">Downvotes <i class="fa fa-arrows-v"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($shows as $show)
                <tr>
                    <td><img style="width: 50px; height: 50px" class="img-responsive" src="{{ asset('images/shows/' . $show->image) }}"/></td>
                    <td>{{$show->title}}</td>
                    <td><i class="fa fa-thumbs-o-up" style="color: green"></i> {{$show->upvotes}} </td>
                    <td><i class="fa fa-thumbs-o-down" style="color: #B40404;"></i> {{$show->downvotes}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            // Handler for .ready() called.
            $('html, body').animate({
                scrollTop: $('#leader').offset().top
            }, 'slow');
        });
    </script>
@endsection