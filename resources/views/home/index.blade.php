@extends('layouts.master')
@section('content')
    <!-- first section -->
    <section class="section1">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-6 wow fadeIn" data-wow-duration="2s">
                <h1 class="section-heading">Welcome to Sky Rate</h1>
                    <hr class="colored">
                <p class="lead section-lead text-center">A community where you can rate the films and TV shows you love. Brought to you by <i>Sky</i>.</p>
                    <hr class="colored">
                    <a class="myButton" href="{{route('rate.index')}}">Let's Go</a>
                </div>
                <div class="col-lg-6 wow slideInRight" data-wow-duration="1s">
                    <img src="http://gmsolutionsholmfirth.co.uk/wp-content/uploads/2014/03/sky-tv.png" >
                </div>
            </div>
        </div>
    </div>
    </section>
            {{--The way I have designed this is such that it's consistent with the Sky.com page. I added this custom aside because otherwise it'd feel empty. --}}
    <!-- aside -->
    {{--<section class="image-bg-fixed-height">--}}
    {{--</section>--}}

<!-- Footer -->
<section class="footer">
    <div class="container">
    <div class="row">
        <div class="col-lg-3 wow fadeIn">
            <h3 style="font-family: Muli, sans-serif; text-align: center">Explore Sky</h3>
            <hr class="gradient-short">
            <p style="text-align: center">Sky Corporate</p>
            <p style="text-align: center">Sky for Business</p>
            <p style="text-align: center">Sky Communal TV</p>
            <p style="text-align: center">Store Locator</p>
            <p style="text-align: center">Work for Sky</p>
            <p style="text-align: center">Safety Centre</p>
            <p style="text-align: center">Advertise With Us</p>
        </div>
        <div class="col-lg-3 wow fadeIn" data-wow-delay="1s">
            <h3 style="font-family: Muli, sans-serif; text-align: center">Sky Initiatives</h3>
            <hr class="gradient-short">
            <p style="text-align: center">Bigger Picture</p>
            <p style="text-align: center">Sky Academy</p>
            <p style="text-align: center">Sky Sports Living For Sport</p>
            <p style="text-align: center">Sky Academy Starting Out</p>
            <p style="text-align: center">Sky Ride</p>
            <p style="text-align: center">Sky Rainforest Rescue</p>
        </div>
        <div class="col-lg-3 wow fadeIn" data-wow-delay="2s">
            <h3 style="font-family: Muli, sans-serif; text-align: center">Sky Channels</h3>
            <hr class="gradient-short">
            <p style="text-align: center">Sky 1</p>
            <p style="text-align: center">Sky Living</p>
            <p style="text-align: center">Sky Atlantic</p>
            <p style="text-align: center">Sky Arts</p>
            <p style="text-align: center">Sky Movies</p>
            <p style="text-align: center">Sky Sports</p>
            <p style="text-align: center">Sky News</p>
        </div>
        <div class="col-lg-3 wow fadeIn" data-wow-delay="3s">
            <h3 style="font-family: Muli, sans-serif; text-align: center">More Sky Sites</h3>
            <hr class="gradient-short">
            <p style="text-align: center">NOW TV</p>
            <p style="text-align: center">Sky Tickets</p>
        </div>
    </div>
    </div>
</section>
@endsection