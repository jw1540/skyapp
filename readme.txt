Hi, 

Thank you for the opportunity to do this unattended test. My code isn't perfect but is functional - I'd love to learn how to make it perfect!

//**Installation**//
The app is built in laravel and requires the following: 
1.
(sudo (if unix)) npm install
(sudo) bower install (if no bower.json => npm install bower)
(sudo) composer install 

{2. 
May not be required*{Clone the .env.example file replacing the database information with relevant info.
May not be required*{I have provided a sql file to be used although a blank database can be user, too. 
{3. php artisan migrate (I have provided a 

4. gulp (if fails install gulp) 

4. php artisan serve 

//**Notes**// 
Laravel cannot run on shared hosting so if uploaded to a server ensure it is a cloud server. 
To prevent the 'no content' I decided it would be more efficient (and visually consistent) to make image upload a mandatory option. 

//**Future Developments (not included in brief)**//
The rating system is flawed in that a user can vote multiple times. 
To prevent this I'd add some user authentication and logic that prohibits multiple rating.
The leaderboard is organised by votes with the additional option to re-order. If they share the same votes, they're organised by date created. 

//**Additional**//
*These steps may not be required as I have provided the env file and sql database. This is usually only the case with git clone.